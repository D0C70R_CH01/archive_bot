/*
SDBX 아카이브 봇

본문은 81줄부터

20181107
--버그 있다길래 고침--
---테스트용버전20180628---
---이름색 감지 추가---
---테스트용버전20180626---
-----이제 이미지로 렌더링해서 박제시킴---
---테스트용버전20180614---
-----dom-to-image 이거 노드자스위에서 안굴러감---
---테스트용버전20180607---
-----dom-to-image 라는거 들고와봄 알려주실분---
-----버그픽스---
---테스트용버전20180523---
-----핑 추가함---
---테스트용버전20180518---
   
                  너
                박-제
   
최근 채팅 50개 까지 저장함.

?도움말, ?헬프, ?help, ?ㅗ디ㅔ : 도움말을 개인채팅으로 보여줌

?목록, ?ㅁㄹ, ?af : 저장되어있는 채팅을 개인채팅으로 보여줌
ex)
A : asdf
B : ㅁㄴㅇㄹ
C : ㅂㅈㄷㄱ
D : qwer
A : ?ㅁㄹ

봇 : (4) A#1234 : asdf
봇 : (3) B#5678 : ㅁㄴㅇㄹ
봇 : (2) C#9012 : ㅂㅈㄷㄱ
봇 : (1) D#3456 : qwer

?스톱, ?ㅅㅌ, ?tx : 채팅 수집을 중지함 한번 더 치거나 박제 명령어를 치면 풀림
ex)
A : asdf
B : ㅁㄴㅇㄹ
B : ?ㅅㅌ
C : ㅂㅈㄷㄱ
A : ?ㅁㄹ

봇 : (2) A#1234 : asdf
봇 : (1) B#5678 : ㅁㄴㅇㄹ

?박제, ?ㅂㅈ, ?qw : 바로 윗 어록 박제
ex)
A : asdf
B: ?ㅂㅈ
봇: A#1234 : asdf

?박제 x, ?ㅂㅈ x, ?qw x : 최근 x번째 채팅을 박제
ex)
A : asdf
B : ㅁㄴㅇㄹ
C: ㅂㅈㄷㄱ
B: ?박제-3
봇: A#1234 : asdf

?박제 x-y, ?ㅂㅈ x-y, ?qw x-y : 최근 x번째 채팅부터 y번째 채팅까지 박제
ex)
ex)
A : asdf
B : ㅁㄴㅇㄹ
C: ㅂㅈㄷㄱ
B: ?박제-2-3
봇: A#1234 : asdf
봇: B#5678 : ㅁㄴㅇㄹ

?핑, ?vld, ?ping : 핑을 출력함

?설치 : 박제봇이 메세지를 출력할 채널을 설정함 설정안하면 출력 안됨
*/

var local = "D:/!DoctorChoi_GitLab/archive_bot/"; //봇이 들어있는 로컬 저장소 위치를 입력하시면 댑니다

var discord = require("discord.js");
var puppeteer = require("puppeteer");
var fs = require("fs");
var client = new discord.Client();
var arrChat = new Array();
var tc = null;
var bStop = false;

var token = fs.readFileSync("token.txt", "utf-8");

client.login(token);

client.on("ready", () =>
{
    console.log(client.user.tag + " : 박제준비끗");
});

client.on("message", (message) =>
{
    if(message.author.tag != client.user.tag)
    {
        var intPar = 0;
        var intPar2 = 0;

        console.log("[" + message.content + "]");

        if(message.content.slice(0, 4) == "?박제 " || message.content.slice(0, 4) == "?ㅂㅈ " || message.content.slice(0, 4) == "?qw ")
        {
            intPar = message.content.slice(4, message.content.length);

            if(intPar.indexOf("-") != -1)
            {
                intPar2 = Number(intPar.slice(intPar.indexOf("-") + 1, intPar.length));

                intPar = Number(intPar.slice(0, intPar.indexOf("-")));
            }

            console.log("[" + intPar + ", " + intPar2 + "]");
        }

       if(message.content != "?ㅗ디ㅔ" && message.content != "?핑" && message.content != "?vld" && message.content != "?ping" && message.content != "?도움말" && message.content != "?헬프" && message.content != "?help" && message.content != "?스톱" && message.content != "?ㅅㅌ" && message.content != "?tx" &&message.content != "?설치" && message.content != "?ㅁㄹ" &&message.content != "?목록" &&message.content != "?af" && message.content != "?ㅂㅈ" &&message.content != "?박제" &&message.content != "?qw" && message.content != "?박제 " + intPar && message.content != "?ㅂㅈ " + intPar && message.content != "?qw " + intPar && message.content != "?박제 " + intPar + "-" + intPar2 && message.content != "?qw " + intPar + "-" + intPar2 && message.content != "?ㅂㅈ " + intPar + "-" + intPar2)
       {
           if(!bStop && tc != null)
           {
               if(arrChat.length >= 50)
               {
                   arrChat.shift();

                    arrChat.push({mes:message, mem:message.member});
                }
                else
                {
                    arrChat.push({mes:message, mem:message.member});
                }
            }
       }
       else if(message.content == "?핑" || message.content == "?vld" || message.content == "?ping" )
       {
            message.channel.send("핑 : " + client.ping + "ms");
       }
       else if(message.content == "?ㅗ디ㅔ" || message.content == "?도움말" || message.content == "?헬프" || message.content == "?help")
       {
            message.author.send("[Archive_BOT]\n" + "========도움말========\n\n\n만든놈: 최박사(https://gitlab.com/DoctorChoi  /n아카이브 봇\n   \n                  너\n                박-제\n   \n최근 채팅 50개 까지 저장함.\n\n?도움말, ?헬프, ?help : 도움말을 개인채팅으로 보여줌\n\n?목록, ?ㅁㄹ, ?af : 저장되어있는 채팅을 개인채팅으로 보여줌\nex)\nA : asdf\nB : ㅁㄴㅇㄹ\nC : ㅂㅈㄷㄱ\nD : qwer\nA : ?ㅁㄹ\n\n봇 : (4) A#1234 : asdf\n봇 : (3) B#5678 : ㅁㄴㅇㄹ\n봇 : (2) C#9012 : ㅂㅈㄷㄱ\n봇 : (1) D#3456 : qwer\n\n?스톱, ?ㅅㅌ, ?tx : 채팅 수집을 중지함 한번 더 치거나 박제 명령어를 치면 풀림\nex)\nA : asdf\nB : ㅁㄴㅇㄹ\nB : ?ㅅㅌ\nC : ㅂㅈㄷㄱ\nA : ?ㅁㄹ\n\n봇 : (2) A#1234 : asdf\n봇 : (1) B#5678 : ㅁㄴㅇㄹ\n\n?박제, ?ㅂㅈ, ?qw : 바로 윗 어록 박제\nex)\nA : asdf\nB: ?ㅂㅈ\n봇: A#1234 : asdf\n\n?박제 x, ?ㅂㅈ x, ?qw x : 최근 x번째 채팅을 박제\nex)\nA : asdf\nB : ㅁㄴㅇㄹ\nC: ㅂㅈㄷㄱ\nB: ?박제-3\n봇: A#1234 : asdf\n\n?박제 x-y, ?ㅂㅈ x-y, ?qw x-y : 최근 x번째 채팅부터 y번째 채팅까지 박제\nex)\nA : asdf\nB : ㅁㄴㅇㄹ\nC: ㅂㅈㄷㄱ\nB: ?박제-2-3\n봇: A#1234 : asdf\n봇: B#5678 : ㅁㄴㅇㄹ\n\n?설치 : 박제봇이 메세지를 출력할 채널을 설정합니다. 설정안하면 출력 안됨\n");
       }
       else if(message.content == "?스톱" || message.content == "?ㅅㅌ" || message.content == "?tx")
       {
            if(bStop)
            {
                bStop = false;
            }
            else
            {
                bStop = true;
            }

            message.channel.send("[Archive_BOT]\n" + "일시정지 = " + bStop);
       }
       else if(message.content == "?목록" || message.content == "?ㅁㄹ" || message.content == "?af")
       {
           var buf = "";

            for(var i = arrChat.length; i >= 1; i--)
           {
                buf += "(" + i + ")" + arrChat[arrChat.length - i].mem.displayName  + " : " + arrChat[arrChat.length - i].mes.content + "\n";
           }
           message.author.send("[Archive_BOT]\n" + "========총 " + arrChat.length + "개의 어록이 있습니다.========\n" + buf);
       }
       else if(message.content == "?설치")
        {
            if(tc != null)
            {
                message.channel.send("[Archive_BOT]\n" + "이미 설치되어 있는것 같은데요?");
            }
            else
            {
                tc = message.channel;

                tc.send("[Archive_BOT]\n" + "설치완료!");
            }
        }
       else if(tc != null)
       {
            if(message.content == "?박제" || message.content == "?ㅂㅈ" || message.content == "?qw")
            {
                if(arrChat.length == 0)
                {
                    message.channel.send("[Archive_BOT]\n" + "박제할게 없는데요?");
                }
                else
                {
                    var dt = '<html><head><link rel="stylesheet" type="text/css" href="test2.css"></head><body style = "font-family: Whitney, Apple SD Gothic Neo, NanumBarunGothic, 맑은 고딕, Malgun Gothic, Gulim, 굴림, Dotum, 돋움, Helvetica Neue, Helvetica, Arial, sans-serif;"><div id="app-mount" class="appMount-3VJmYg"><div class="app-19_DXt platform-web"><div class="app flex-vertical theme-dark"><div class="message-group hide-overflow compact"><div class="comment">';

                    puppeteer.launch().then(async browser =>
                        {
                            var strNm = arrChat[arrChat.length - 1].mem.displayName + " ";
                            var strCol = arrChat[arrChat.length - 1].mem.displayHexColor;
                            var strCt = arrChat[arrChat.length - 1].mes.content;
                    
                            dt += '<div class="message-text"><div class="markup"></span><span class="username-wrapper"><strong class="user-name" style = "color:'+ strCol +'">' + strNm + '</strong><i class="highlight-separator right-pad"> : </i></span><span class="message-content">' + strCt + '</span></div></div>';
                            
                            dt += '</div></div></div></div></div></body></html>';

                            await fs.writeFileSync("archive.html", dt, 'utf-8');

                            try
                            {
                                var page = await browser.newPage();

                                await page.goto("file:///"+ local +"archive.html");
                                await page.screenshot({path : "archive.png", clip : {x : 10, y : 0, width : 500, height : 35}});
                                
                                await tc.sendFile("archive.png", "archive.png");
                            }
                            catch(e)
                            {
                                //아무 일도 일어나지 않는다.
                            }
                        });

                    console.log(client.user.tag + " : " + "박제작동");
                    bStop = false;
                }
            }
            else if(message.content == "?박제 " + intPar || message.content == "?ㅂㅈ " + intPar || message.content == "?qw " + intPar)
            {
                if(intPar < 0 || intPar > 50)
                {
                    message.channel.send("[Archive_BOT]\n" + "인자가 잘못된것 같은데요?");
                }
                else if(arrChat.length < intPar)
                {
                    message.channel.send("[Archive_BOT]\n" + "박제할게 없는데요?");
                }
                else
                {
                    var dt = '<html><head><link rel="stylesheet" type="text/css" href="test2.css"></head><body style = "font-family: Whitney, Apple SD Gothic Neo, NanumBarunGothic, 맑은 고딕, Malgun Gothic, Gulim, 굴림, Dotum, 돋움, Helvetica Neue, Helvetica, Arial, sans-serif;"><div id="app-mount" class="appMount-3VJmYg"><div class="app-19_DXt platform-web"><div class="app flex-vertical theme-dark"><div class="message-group hide-overflow compact"><div class="comment">';

                    puppeteer.launch().then(async browser =>
                        {
                            var strNm = arrChat[arrChat.length - intPar].mem.displayName + " ";
                            var strCol = arrChat[arrChat.length - intPar].mem.displayHexColor;
                            var strCt = arrChat[arrChat.length - intPar].mes.content;
                    
                            dt += '<div class="message-text"><div class="markup"></span><span class="username-wrapper"><strong class="user-name" style = "color:'+ strCol +'">' + strNm + '</strong><i class="highlight-separator right-pad"> : </i></span><span class="message-content">' + strCt + '</span></div></div>';
                            
                            dt += '</div></div></div></div></div></body></html>';

                            await fs.writeFileSync("archive.html", dt, 'utf-8');

                            try
                            {
                                var page = await browser.newPage();

                                await page.goto("file:///"+ local +"archive.html");
                                await page.screenshot({path : "archive.png", clip : {x : 10, y : 0, width : 500, height : 35}});

                                await tc.sendFile("archive.png", "archive.png");
                            }
                            catch(e)
                            {
                                //아무 일도 일어나지 않는다.
                            }
                        });

                    console.log(client.user.tag + " : " + "박제작동");
                    bStop = false;
                }
            }
            else if(message.content == "?박제 " + intPar + "-" + intPar2 || message.content == "?ㅂㅈ " + intPar + "-" + intPar2 || message.content == "?qw " + intPar + "-" + intPar2)
            {
                if(intPar < 0 || intPar > 50 || intPar2 < 0 || intPar2 > 50 || intPar > intPar2)
                {
                    message.channel.send("[Archive_BOT]\n" + "인자가 잘못된것 같은데요?");
                }
                else if(arrChat.length < intPar || arrChat.length < intPar2)
                {
                    message.channel.send("[Archive_BOT]\n" + "박제할게 없는데요?");
                }
                else
                {
                    var dt = '<html><head><link rel="stylesheet" type="text/css" href="test2.css"></head><body style = "font-family: Whitney, Apple SD Gothic Neo, NanumBarunGothic, 맑은 고딕, Malgun Gothic, Gulim, 굴림, Dotum, 돋움, Helvetica Neue, Helvetica, Arial, sans-serif;"><div id="app-mount" class="appMount-3VJmYg"><div class="app-19_DXt platform-web"><div class="app flex-vertical theme-dark"><div class="message-group hide-overflow compact"><div class="comment">';
                    var n = 0;

                    puppeteer.launch().then(async browser =>
                        {

                            for(var i = intPar2; i >= intPar; i--)
                            {
                                n += 25;

                            var strNm = arrChat[arrChat.length - i].mem.displayName + " ";
                            var strCol = arrChat[arrChat.length - i].mem.displayHexColor;
                            var strCt = arrChat[arrChat.length - i].mes.content;
                    
                            dt += '<div class="message-text"><div class="markup"></span><span class="username-wrapper"><strong class="user-name" style = "color:'+ strCol +'">' + strNm + '</strong><i class="highlight-separator right-pad"> : </i></span><span class="message-content">' + strCt + '</span></div></div>';
                            }

                            dt += '</div></div></div></div></div></body></html>';

                            await fs.writeFileSync("archive.html", dt, 'utf-8');

                            try
                            {
                                var page = await browser.newPage();

                                await page.goto("file:///"+ local +"archive.html");
                                await page.screenshot({path : "archive.png", clip : {x : 10, y : 0, width : 500, height : n}});

                                await tc.sendFile("archive.png", "archive.png");
                            }
                            catch(e)
                            {
                                //아무 일도 일어나지 않는다.
                            }
                        });

                    console.log(client.user.tag + " : " + "박제작동");
                    bStop = false;
                }
            }
        }
    }
});